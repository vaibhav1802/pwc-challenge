// This re-usable component is responsible for accepting text inputs for todo list
import React from 'react';
import PropTypes from 'prop-types';

const TextInput = (props) => {
  const {name, value, onInputType, label} = props;
  return (
    <label> {label}
      <input type="text"
        className="input-text"
        name={name} 
        value={value}
        placeholder={'Enter the task'}
        onChange={(e) => onInputType(e)} />
    </label>   
  )
};

TextInput.propTypes = {
  label: PropTypes.string,
  value: PropTypes.string,
  name: PropTypes.string,
  onInputType: PropTypes.func
};

export default TextInput;