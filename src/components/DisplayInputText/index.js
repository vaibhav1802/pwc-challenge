import React from 'react';
import PropTypes from 'prop-types';

const DisplayInputText = (props) => {
  const {inputText} = props;
  return (
    <div className="live-input-wrapper">
      <h3 className="title-input-type">See what you are typing ....</h3>
      <p className="input-text-live">{inputText}</p>
    </div>
  )
};

DisplayInputText.propTypes = {
  inputText: PropTypes.string
};

export default DisplayInputText;