// This component is responsible for accepting priority of todo list item
import React from 'react';
import PropTypes from 'prop-types';

const Select = (props) => {
  const {items, onSelectChange, label} = props;
  return (
    <div className="select-wrapper">
      <label>{label}
        <select className="select" onChange={(e) => onSelectChange(e)}>
          {
            items.map((item, count) => 
              <option key={count} value={item.priority}>{item.priorityLabel}</option>
            )
          }
        </select>
      </label>
    </div>
  )
};

Select.propTypes = {
  items: PropTypes.array,
  onSelectChange: PropTypes.func,
  label: PropTypes.string
};

export default Select;

