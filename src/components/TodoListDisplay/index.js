// This component is responsible for rendering the added to do list
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import config from '../../config/config.json';
import calculateNextSortingState from '../../utilities/calculateNextSortState';

export default class TodoListDisplay extends Component {
  constructor(props) {
    super(props);
    this.state = {
      prioritySorting: 'desc',
      taskNameSorting: 'asc'
    }
  }

  getPriorityLabel(taskPriority) {
    return config.taskPriorityModel.filter((item) => item.priority === taskPriority)[0].priorityLabel;
  }

  getCompleteTaskClass(item) {
    return item.completed ? 'completed': 'incomplete';
  }

  // sort the list by task priority
  sortByPriority() {
    const priorityOrder = calculateNextSortingState(this.state.prioritySorting);
    this.props.onPrioritySort(priorityOrder);
    this.setState({
      prioritySorting: priorityOrder
    });
  }

  // sort the list by task name
  sortByName() {
    const taskNameOrder = calculateNextSortingState(this.state.taskNameSorting);
    this.props.onTaskNameSort(taskNameOrder);
    this.setState({
      taskNameSorting: taskNameOrder
    });
  }

  // return the count of completed task
  findCompletedTasksCount() {
    const completedTaskCount = this.props.todoList.reduce((accumulator, item) => {
      return item.completed ? accumulator + 1 : accumulator
    }, 0);
    return completedTaskCount;
  }

  render() {
    const {todoList, onDelete, onComplete} = this.props;
    return (
      <React.Fragment>
        <span>Filter By: </span>
        <button className="btn sorting" onClick={() => this.sortByPriority()}>Priority</button>
        <button className="btn sorting" onClick={() => this.sortByName()}>Task Name</button>
        <ul>
          {
            todoList.map((list) => 
              <li className={`list-item ${this.getCompleteTaskClass(list)}`} id={list.todoId} key={list.todoId}>
                <div>
                  <input className="complete-toggle" type="checkbox" onClick={() => onComplete(list.todoId)}></input>
                  <span className="task-name">{list.todoName}</span>
                  <p className="priority">Priority: {this.getPriorityLabel(list.todoPriority)}
                    <button onClick={() => onDelete(list.todoId)} className="btn delete">Delete</button>
                  </p>
                </div>
              </li>
            )
          }
        </ul>
        <span className="task-count">Total Tasks: {todoList.length}</span>
        <span className="task-count">Completed Tasks: {this.findCompletedTasksCount()}</span>
      </React.Fragment>
    );
  }
};

TodoListDisplay.propTypes = {
  todoList: PropTypes.array,
  onDelete: PropTypes.func,
  onComplete: PropTypes.func,
  onPrioritySort: PropTypes.func,
  onTaskNameSort: PropTypes.func
};