import React from 'react';
import { mount, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import TodoInput from './index';

configure({ adapter: new Adapter() });

let saved = {};

describe('<TodoInput />', () => {
  beforeEach(() => {
    saved = {
      spies: {
        onInputType: jest.spyOn(TodoInput.prototype, 'onInputType')
      },
      wrapper: mount(<TodoInput />)
    };
  });
  afterEach(() => {
    saved.spies.onInputType.mockRestore();
    saved.wrapper.unmount();
    saved = {};
  });
  describe('Interactions with text box', () => {
    test('When text change event is triggered', () => {
      saved.wrapper.find('.input-text').simulate('change');
      expect(saved.spies.onInputType).toHaveBeenCalledTimes(1);
    });
    test('Check for placeholder text', () => {
      const input = saved.wrapper.find('.input-text');
      expect(input.get(0).props.placeholder).toEqual('Enter the task');
    });
  });
});