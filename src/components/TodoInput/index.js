// Form component to capture user input for todo list
import React, { Component } from 'react';
import Select from '../Select';
import TextInput from '../TextInput';
import DisplayInputText from '../DisplayInputText';
import config from '../../config/config.json';
import PropTypes from 'prop-types';

export default class TodoInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      taskPriority: 3,
      taskId: 1,
      inputValue: ''
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onSelectChange = this.onSelectChange.bind(this);
    this.onInputType = this.onInputType.bind(this);
  }

  onSelectChange(e) {
    this.setState({
      taskPriority: e.target.value
    });
  }

  onInputType(e) {
    this.setState({
      inputValue: e.target.value
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    const inputText = this.state.inputValue;
    
    if (inputText !== '') {
      this.setState((state) => ({
        taskId: state.taskId + 1
      }), this.createItem(inputText));

      this.setState({
        inputValue: ''
      });
    }
  }

  createItem(inputVal) {
    const item = {
      todoName: inputVal,
      todoPriority: Number(this.state.taskPriority),
      todoId: this.state.taskId,
      completed: false
    };
    
    this.props.addTask(item);
  }

  render() {
    const {inputValue} = this.state; 
    return(
      <React.Fragment>
        <form className="todo-input-form" onSubmit={this.handleSubmit}>
          <Select items={config.taskPriorityModel} 
            label={"Select the task priority:"}
            onSelectChange={this.onSelectChange} 
          />
          <TextInput name={"todo-input"} 
            label={"Enter the todo task:"}
            onInputType={this.onInputType}
            value={inputValue}
          />
          <input className="btn add-task" type="submit" value="ADD"></input>
        </form>
        <DisplayInputText inputText={inputValue}/>
      </React.Fragment>
    );
  }
};

TodoInput.propTypes = {
  addTask: PropTypes.func
};