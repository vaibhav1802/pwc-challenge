import React, { Component } from 'react';
import TodoInput from '../../components/TodoInput';
import TodoListDisplay from '../../components/TodoListDisplay';
import SortingNumberEngine from '../../utilities/sortByNumber';
import SortingTextEngine from '../../utilities/sortByText';

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      todoList: []
    };
    this.addTask = this.addTask.bind(this);
    this.deleteTask = this.deleteTask.bind(this);
    this.onComplete = this.onComplete.bind(this);
    this.onPrioritySort = this.onPrioritySort.bind(this);
    this.onTaskNameSort = this.onTaskNameSort.bind(this);
  }

  addTask(newItem) {
    const items = [...this.state.todoList, newItem];
    this.setState({
      todoList: items
    });
  }

  deleteTask(id) {
    const updatedList = this.state.todoList.filter((item) => item.todoId !== id);
    this.setState({
      todoList: updatedList
    });
  }

  onComplete(id) {
    const updatedList = this.state.todoList.map((item) => {
      if (item.todoId === id) {
        item.completed = !item.completed;
      }
      return {
        ...item
      }
    });
    
    this.setState({
      todoList: updatedList
    });
  }

  onPrioritySort(sortOrder) {
    const sortedPriorityList = SortingNumberEngine(sortOrder, 'todoPriority', this.state.todoList);
    this.setState({
      todoList: sortedPriorityList
    });
  }

  onTaskNameSort(sortOrder) {
    const sortedTaskNameList = SortingTextEngine(sortOrder, 'todoName', this.state.todoList);
    this.setState({
      todoList: sortedTaskNameList
    });
  }

  render() {
    return(
      <div id="home">
        <h1 className="title">To Do App</h1>
        <div className="flex-parent home-content">
          <div className="todo-input-wrapper">
            <TodoInput addTask={this.addTask} />
          </div>
          <div className="list-display-wrapper">
            <h2>To Do Items:</h2>
            {
              this.state.todoList.length > 0 &&
              <TodoListDisplay todoList={this.state.todoList} 
                onDelete={this.deleteTask}
                onComplete={this.onComplete}
                onPrioritySort={this.onPrioritySort}
                onTaskNameSort={this.onTaskNameSort}
              />
            }
          </div>
        </div>
      </div>
    );
  }
};