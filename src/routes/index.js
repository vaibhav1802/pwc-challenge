import React from 'react';
import {Route, Redirect, Switch} from 'react-router-dom';
import Home from '../pages/Home';

const applicationRoutes = (
  <Switch>
    <Route name="home" path="/home" component={Home} />
    <Redirect exact from="/" to="home" />
  </Switch>
);

export default applicationRoutes;
