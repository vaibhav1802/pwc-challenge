//Sort engine when sort attribute is of type number
import isArray from 'lodash/isArray';

export const SORTING_TYPE = {
  ASC: 'asc',
  DESC: 'desc'
};

export default (sortOrder, sortAttribute, items) => {
  if (!items || !isArray(items)) {
    return [];
  }
  // Sort by ascending order
  if (sortOrder === SORTING_TYPE.ASC) {
    return items.sort((a, b) => a[sortAttribute]-b[sortAttribute]);
  }
  // Sort by descending order
  if (sortOrder === SORTING_TYPE.DESC) {
    return items.sort((a, b) => b[sortAttribute]-a[sortAttribute]);
  }
  return items;
}
