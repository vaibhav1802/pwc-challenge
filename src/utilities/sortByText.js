//Sort engine when sort attribute is of type text
import isArray from 'lodash/isArray';
export const SORTING_TYPE = {
  ASC: 'asc',
  DESC: 'desc'
};

export default (sortOrder, sortAttribute, items) => {
  if (!items || !isArray(items)) {
    return [];
  }
  // Sort by ascending order
  if (sortOrder === SORTING_TYPE.ASC) {
    return items.sort((a, b) => {
      if (!a[sortAttribute]) {
        return -1;
      }
      if (!b[sortAttribute]) {
        return 1;
      }
      return a[sortAttribute].localeCompare(b[sortAttribute], {ignorePunctuation: true})
    });
  }
  // Sort by descending order
  if (sortOrder === SORTING_TYPE.DESC) {
    return items.sort((a, b) => {
      if (!a[sortAttribute]) {
        return 1;
      }
      if (!b[sortAttribute]) {
        return -1;
      }
      return b[sortAttribute].localeCompare(a[sortAttribute], {ignorePunctuation: true});
    });
  }
  return items;
}
