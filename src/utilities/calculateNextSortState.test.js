import calculateNextSortState from './calculateNextSortState';

describe('Test for calculateNextSortState utility function', () => {
  test('Check next sorting state with descending input', () => {
    expect(calculateNextSortState('desc')).toEqual('asc');
  });
  test('Check next sorting state with ascending input', () => {
    expect(calculateNextSortState('asc')).toEqual('desc');
  });
});