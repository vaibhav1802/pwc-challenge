import sortByText from './sortByText';
describe('Test for sortByText utility function', () => {
  test('Check for ascending order', () => {
    const taskLists = [
      {
        todoName: 'Deep diving',
        todoPriority: 2,
        todoId: 1,
        completed: false
      },
      {
        todoName: 'Bird Watching',
        todoPriority: 3,
        todoId: 2,
        completed: false
      },
      {
        todoName: 'Swimming',
        todoPriority: 1,
        todoId: 3,
        completed: false
      }
    ];
    expect(sortByText('asc', 'todoName', taskLists)).toEqual([
      {
        todoName: 'Bird Watching',
        todoPriority: 3,
        todoId: 2,
        completed: false
      },
      {
        todoName: 'Deep diving',
        todoPriority: 2,
        todoId: 1,
        completed: false
      },
      {
        todoName: 'Swimming',
        todoPriority: 1,
        todoId: 3,
        completed: false
      }
    ])
  });
  test('Check for descending order', () => {
    const taskLists = [
      {
        todoName: 'Deep diving',
        todoPriority: 2,
        todoId: 1,
        completed: false
      },
      {
        todoName: 'Bird Watching',
        todoPriority: 3,
        todoId: 2,
        completed: false
      },
      {
        todoName: 'Swimming',
        todoPriority: 1,
        todoId: 3,
        completed: false
      }
    ];
    expect(sortByText('desc', 'todoName', taskLists)).toEqual([
      {
        todoName: 'Swimming',
        todoPriority: 1,
        todoId: 3,
        completed: false
      },
      {
        todoName: 'Deep diving',
        todoPriority: 2,
        todoId: 1,
        completed: false
      },
      {
        todoName: 'Bird Watching',
        todoPriority: 3,
        todoId: 2,
        completed: false
      }
    ]);
  });

  test('Check when data is already sorted in asc', () => {
    const taskLists = [
      {
        todoName: 'Bird Watching',
        todoPriority: 3,
        todoId: 2,
        completed: false
      },
      {
        todoName: 'Deep diving',
        todoPriority: 2,
        todoId: 1,
        completed: false
      }
    ];
    expect(sortByText('asc', 'todoName', taskLists)).toEqual([
      {
        todoName: 'Bird Watching',
        todoPriority: 3,
        todoId: 2,
        completed: false
      },
      {
        todoName: 'Deep diving',
        todoPriority: 2,
        todoId: 1,
        completed: false
      }
    ]);
  });

  test('Check data list is empty with desc', () => {
    const taskLists = [];
    expect(sortByText('desc', 'todoName', taskLists)).toEqual([]);
  });
});