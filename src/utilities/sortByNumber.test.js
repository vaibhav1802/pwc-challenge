import sortByNumber from './sortByNumber';
describe('Test for sortByNumber utility function', () => {
  test('Check for ascending order', () => {
    const taskLists = [
      {
        todoName: 'First Task',
        todoPriority: 2,
        todoId: 1,
        completed: false
      },
      {
        todoName: 'Second Task',
        todoPriority: 3,
        todoId: 2,
        completed: false
      },
      {
        todoName: 'Third Task',
        todoPriority: 1,
        todoId: 3,
        completed: false
      }
    ];
    expect(sortByNumber('asc', 'todoPriority', taskLists)).toEqual([
      {
        todoName: 'Third Task',
        todoPriority: 1,
        todoId: 3,
        completed: false
      },
      {
        todoName: 'First Task',
        todoPriority: 2,
        todoId: 1,
        completed: false
      },
      {
        todoName: 'Second Task',
        todoPriority: 3,
        todoId: 2,
        completed: false
      }
    ])
  });
  test('Check for descending order', () => {
    const taskLists = [
      {
        todoName: 'First Task',
        todoPriority: 2,
        todoId: 1,
        completed: false
      },
      {
        todoName: 'Second Task',
        todoPriority: 3,
        todoId: 2,
        completed: false
      },
      {
        todoName: 'Third Task',
        todoPriority: 1,
        todoId: 3,
        completed: false
      }
    ];
    expect(sortByNumber('desc', 'todoPriority', taskLists)).toEqual([
      {
        todoName: 'Second Task',
        todoPriority: 3,
        todoId: 2,
        completed: false
      },
      {
        todoName: 'First Task',
        todoPriority: 2,
        todoId: 1,
        completed: false
      },
      {
        todoName: 'Third Task',
        todoPriority: 1,
        todoId: 3,
        completed: false
      }
    ]);
  });

  test('Check when data is already sorted in asc', () => {
    const taskLists = [
      {
        todoName: 'Third Task',
        todoPriority: 1,
        todoId: 3,
        completed: false
      },
      {
        todoName: 'First Task',
        todoPriority: 2,
        todoId: 1,
        completed: false
      }
    ];
    expect(sortByNumber('asc', 'todoPriority', taskLists)).toEqual([
      {
        todoName: 'Third Task',
        todoPriority: 1,
        todoId: 3,
        completed: false
      },
      {
        todoName: 'First Task',
        todoPriority: 2,
        todoId: 1,
        completed: false
      }
    ]);
  });
  test('Check data list is empty with desc', () => {
    const taskLists = [];
    expect(sortByNumber('desc', 'todoPriority', taskLists)).toEqual([]);
  });
});