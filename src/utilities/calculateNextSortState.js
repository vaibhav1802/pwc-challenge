// We can sort by ascending or descending order. This func will give us the next sorting order

export default function calculateNextSortState(currentSorting) {
  const sortingOptions = ['asc', 'desc'];
  const index = sortingOptions.indexOf(currentSorting);
  // Return the next sorting options in array, go back to return the first option if reach the end
  const updatedIndex = (index + 1 === sortingOptions.length) ? 0 : (index + 1);
  return sortingOptions[updatedIndex];
}