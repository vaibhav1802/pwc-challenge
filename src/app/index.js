import React, { Component } from 'react';
import {HashRouter as Router} from 'react-router-dom';
import routes from '../routes';

class App extends Component {
  render() {
    return (
      <div className="app-wrapper">
        <Router>
          {routes}
        </Router>
      </div>
    );
  }
}

export default App;