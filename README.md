PWC CODE CHALLENGE:

THINGS THAT COULD HAVE BEEN IMPROVED IF TIME ALLOWS:
1. More Component Unit Tests
2. Better Styling

PROJECT DEPENDENCIES:
1. React Router: For routing (Not required in this app, but just for showcasing router)

2. Jest and Enzyme: For unit test

3. Proptypes for typechecking

4. Webpack server for dev mode and bundling


## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:8080] to view it in the browser.

### `npm test`
Launches the test runner.


## Project Requirements
Your task
You have been asked to develop a to-do list application that allows the user to create and manage tasks. The application has the following features:
• Add and view tasks
• Delete a task
• Complete a task
• Set a priority for my tasks
• View the tasks sorted by priority and name
• View the number of total and completed tasks

